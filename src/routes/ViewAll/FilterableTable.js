import React from 'react'
import TransactionRow from './TransactionRow'

export const FilterableTable = ({
	elements,
	filterText,
	headers,
	className,
	filterableAttributes,
	deleteTransaction,
}: {
	elements: Object[],
	filterText: string,
	headers: {
		content: string,
		className: string,
	}[],
	className?: string,
	filterableAttributes: string[],
	deleteTransaction: (id: string) => void,
}) => {
	let classNames = 'FilterableTable'
	if (className) classNames += ' ' + className
	return (
		<div className={classNames}>
			<div className="header row">
				{headers.map(header => (
					<div className={header.className} key={header.content}>
						{header.content}
					</div>
				))}
			</div>
			<div className="body">
				{elements
					.filter(element =>
						filterableAttributes.some(attribute =>
							element[attribute].toLowerCase().includes(filterText.toLowerCase())
						)
					)
					.map(element => (
						<TransactionRow
							transaction={element}
							key={element.id}
							removeTransaction={() => deleteTransaction(element.id)}
						/>
					))}
			</div>
		</div>
	)
}

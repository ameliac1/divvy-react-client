import React, { Component } from 'react'
import { connect } from 'react-redux'
import { login, register } from '../../store/user'
import { AnimatedButton } from '../../components'
import './Login.css'
import '../../Buttons.css'

type Props = {
	login: (email: string, password: string) => mixed,
	register: (email: string, password: string, firstName: string, lastName: string) => mixed,
}

type State = {
	email: string,
	password: string,
	firstName: string,
	lastName: string,
	error: ?string,
	isRegistering: boolean,
}

class Login extends Component<Props, State> {
	state = {
		error: null,
		email: '',
		password: '',
		isRegistering: false,
		firstName: '',
		lastName: '',
	}

	render() {
		const { error, email, password, isRegistering, firstName, lastName } = this.state

		return (
			<div className="Login">
				<form onSubmit={this.onSubmit}>
					{this.renderHeader()}
					{error && <div className="error">{error}.</div>}
					{isRegistering && (
						<div className="row">
							<div className="fieldset">
								<div className="placeholder">First Name</div>
								<input
									id="firstName"
									type="text"
									placeholder=" "
									value={firstName}
									onChange={this.handleFieldChange('firstName')}
								/>
							</div>
							<div className="fieldset">
								<div className="placeholder">Last Name</div>
								<input
									id="lastName"
									type="text"
									placeholder=" "
									value={lastName}
									onChange={this.handleFieldChange('lastName')}
								/>
							</div>
						</div>
					)}
					<div className="fieldset">
						<div className="placeholder">Email</div>
						<input
							id="email"
							type="text"
							placeholder=" "
							value={email}
							onChange={this.handleFieldChange('email')}
						/>
					</div>
					<div className="fieldset">
						<div className="placeholder">Password</div>
						<input
							id="password"
							type="password"
							placeholder=""
							value={password}
							onChange={this.handleFieldChange('password')}
						/>
					</div>
					<AnimatedButton type="submit" content="Submit" />
				</form>
			</div>
		)
	}

	renderHeader() {
		return this.state.isRegistering ? (
			<React.Fragment>
				<h1>Create an Account</h1>
				<h3>
					Already have an account?{' '}
					<span className="register-link" onClick={() => this.setIsRegistering(false)}>
						Back to Login
					</span>
				</h3>
			</React.Fragment>
		) : (
			<React.Fragment>
				<h1>Welcome.</h1>
				<h3>
					New user?{' '}
					<span className="register-link" onClick={() => this.setIsRegistering(true)}>
						Click to register
					</span>
				</h3>
			</React.Fragment>
		)
	}

	handleFieldChange = (fieldName: string) => (e: MessageEvent) => {
		this.setState({
			error: null,
			[fieldName]: e.target.value,
		})
	}

	setError(error: { response?: { data: { message?: string } } }) {
		if (error.response && error.response.data.message) {
			this.setState({
				error: error.response.data.message,
			})
		} else {
			this.setState({ error: 'Uh oh! Something went wrong' })
		}
	}

	onSubmit = async (e: MessageEvent) => {
		const { email, password, firstName, lastName, isRegistering } = this.state

		e.preventDefault()

		if (!email) {
			this.setState({ error: 'Please enter your email' })
			return
		}

		if (!password) {
			this.setState({ error: 'Please enter your password' })
			return
		}

		if (isRegistering) {
			if (!firstName) {
				this.setState({ error: 'Please enter your first name' })
				return
			}

			if (!lastName) {
				this.setState({ error: 'Please enter your last name' })
				return
			}

			try {
				this.props.register(email, password, firstName, lastName)
			} catch (err) {
				this.setError(err)
			}
		} else {
			try {
				this.props.login(email, password)
			} catch (err) {
				this.setError(err)
			}
		}
	}

	setIsRegistering(registering: boolean) {
		this.setState({ isRegistering: registering })
	}
}

const mapDispatchToProps = {
	login,
	register,
}

export default connect(
	null,
	mapDispatchToProps
)(Login)

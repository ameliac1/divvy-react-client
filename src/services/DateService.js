const months = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December',
]

/**
 * Format string date takes a string date and returns a readable string
 * @return {String} A displayable date in string format
 */
function formatStringDateLong(stringDate: ?string): string {
	if (stringDate) {
		const date = new Date(stringDate)
		return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear()
	} else return ''
}

/**
 * Format date takes a date object and returns a readable string in format 'Month dd, yyyy'
 * @return {String} A displayable date in string format
 */
function formatDateLong(date: ?Date): string {
	if (date) {
		return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear()
	} else return ''
}

/**
 * Format date takes a date object and returns a readable string in format 'mm/dd/yyyy'
 * @return {String} A displayable date in string format
 */
function formatStringDateShort(stringDate: ?string): string {
	if (stringDate) {
		const date = new Date(stringDate)
		return (
			(date.getMonth() + 1).toString() +
			'/' +
			date.getDate() +
			'/' +
			date
				.getFullYear()
				.toString()
				.substring(2, 4)
		)
	} else return ''
}

/**
 * Format date takes a date object and returns a readable string in format 'mm/dd/yyyy'
 * @return {String} A displayable date in string format
 */
function formatDateShort(date: ?Date): string {
	if (date) {
		return (
			(date.getMonth() + 1).toString() +
			'/' +
			date.getDate() +
			'/' +
			date
				.getFullYear()
				.toString()
				.substring(2, 4)
		)
	} else return ''
}

module.exports = {
	formatStringDateLong,
	formatDateLong,
	formatStringDateShort,
	formatDateShort,
}

import React, { Component } from 'react'
import { Provider } from 'react-redux'
import Main from './Main'
import { getUser } from './store/user'
import { getCategories, getMethods, getCurrencies } from './store/transaction'
import store from './store'
import 'react-datepicker/dist/react-datepicker.css'
import 'react-toastify/dist/ReactToastify.min.css'
import './shared.css'

export default class App extends Component<{}> {
	componentDidMount() {
		store.dispatch(getUser())
		store.dispatch(getCategories())
		store.dispatch(getMethods())
		store.dispatch(getCurrencies())
	}

	render() {
		return (
			<Provider store={store}>
				<Main />
			</Provider>
		)
	}
}

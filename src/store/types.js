export type Action = {
	type: string,
	payload?: mixed,
}

export type IdMap<T> = { [id: string]: T }

type SimpleUser = {
	id: number,
	firstName: string,
	lastName: string,
}

export type User = {
	...SimpleUser,
	email: string,
}

export type Currency = {
	id: number,
	code: string, // e.g 'USD'
	name: string,
	unicode: string,
}

export type Option = {
	id: number,
	name: string,
}

export type Transaction = {
	id: number,
	user: SimpleUser,
	date: string,
	title: string,
	majorAmount: number, // 'dollars'
	minorAmount: number, // 'cents'
	currency: Currency,
	category: Option,
	method: Option,
	note?: string,
}

export type NewTransaction = {
	date: Date,
	title: string,
	majorAmount: number, // 'dollars'
	minorAmount: number, // 'cents'
	currency: Currency,
	category: ?Option,
	method: Option,
	note?: string,
	user: User,
}

export type TransactionStore = {
	all: IdMap<Transaction>,
	categories: IdMap<Option>,
	methods: IdMap<Option>,
	currencies: IdMap<Currency>,
}

export type UserStore = User

export type GetState = () => mixed

import React, { Component } from 'react'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import NewTransactionRow from './NewTransactionRow'
import { toast } from 'react-toastify'
import { findObject, newTransactionIsValid } from '../../services/Helpers'
import type { IdMap, Category, Method, Currency, NewTransaction, User } from '../../store/types'
import { saveBulkTransactions } from '../../store/transaction'
import { FileLoader } from '../../components'
import './UploadFile.css'

const EXAMPLE_CSV = [
	['Title', 'Date', 'Dollars', 'Cents', 'Currency', 'Method', 'Category', 'Note'],
	['Walmart', '01/01/2019', '60', '23', 'USD', 'Credit', 'Food', 'basic groceries'],
	[
		'Amazon.com',
		'01/03/2019',
		'27',
		'32',
		'USD',
		'Credit',
		'Entertainment',
		'Monty Python + the Holy Grail DVD',
	],
	['January Paycheck #1', '01/06/2019', '100', '00', 'USD', 'Direct Deposit', 'Income', ''],
]

type Props = {
	categories: IdMap<Category>,
	methods: IdMap<Method>,
	currencies: IdMap<Currency>,
	user: User,
	saveBulkTransactions: (transactions: NewTransaction[]) => void,
	history: Object,
}

type State = {
	newTransactions: any[],
}

class UploadFile extends Component<Props, State> {
	state = {
		newTransactions: [],
	}

	render() {
		return (
			<div className="UploadFile">
				<h1>Upload File</h1>
				{this.state.newTransactions.length > 0 ? (
					<React.Fragment>
						Your file contained {this.state.newTransactions.length} transactions:
						{this.state.newTransactions.map((transaction, i) => (
							<NewTransactionRow
								transaction={transaction}
								key={`${transaction.title}-${i}`}
								currencies={this.props.currencies}
								methods={this.props.methods}
								categories={this.props.categories}
								handleDateChange={date => this.handleDateChange(date, i)}
								handleFieldChange={field => this.handleFieldChange(field, i)}
							/>
						))}
						<button className="slidey-sides" onClick={this.save}>
							<span>Save All</span>
						</button>
					</React.Fragment>
				) : (
					<React.Fragment>
						<p className="note">
							Here, you can upload a .csv file to import transactions in bulk. The file should be
							formatted like the downloadable example, with the correct column titles. If you are
							having trouble uploading your own file, try downloading the example csv then replace
							the example transaction data with your own and upload. If the file won{"'"}t download,
							try allowing popups from this site. <br />
							<br />
							All columns are required, except for note. Please ensure the date for each transaction
							is in the format {"'"}MM/DD/YYYY{"'"}. Currency should be the all-capital, three
							letter currency code (i.e. {"'"}USD{"'"}). The {"'"}Dollars{"'"} should include no
							decimals or commas.
						</p>
						<FileLoader onLoad={this.setTransactions} exampleData={EXAMPLE_CSV} />
						<div className="row options">
							<div>
								Category options are:
								<ul>
									{Object.values(this.props.categories).map(category => (
										<li key={category.id}>{category.name}</li>
									))}
								</ul>
							</div>
							<div>
								Method options are:
								<ul>
									{Object.values(this.props.methods).map(method => (
										<li key={method.id}>{method.name}</li>
									))}
								</ul>
							</div>
						</div>
					</React.Fragment>
				)}
			</div>
		)
	}

	setTransactions = (data: any[]) => {
		const transactions = data.map(transaction => ({
			user: this.props.user,
			date: transaction.date ? new Date(transaction.date) : new Date(),
			title: transaction.title || '',
			majorAmount: transaction.dollars || 0,
			minorAmount: transaction.cents || '00',
			currency:
				this.getObjectByKey(this.props.currencies, 'code', transaction.currency) ||
				this.props.currencies[10],
			category:
				this.getObjectByKey(this.props.categories, 'name', transaction.category) ||
				this.props.categories[12],
			method:
				this.getObjectByKey(this.props.methods, 'name', transaction.method) ||
				this.props.methods[1],
			note: transaction.note,
		}))
		this.setState({ newTransactions: transactions })
	}

	getObjectByKey(searchObject: Object, key: string, matchValue: string): ?Object {
		return findObject(searchObject, item => {
			return item.hasOwnProperty(key) && item[key] === matchValue
		})
	}

	handleDateChange = (date: Date, index: number) => {
		this.setState(state => {
			return {
				newTransactions: update(state.newTransactions, {
					[index]: { date: { $set: date } },
				}),
			}
		})
	}

	handleFieldChange = (fieldName: string, index: number) => (e: MessageEvent) => {
		e.persist()
		let updatedTransactions = update(this.state.newTransactions, {
			[index]: { [fieldName]: { $set: e.target.value } },
		})
		this.setState(state => {
			return {
				newTransactions: update(updatedTransactions, {
					[index]: { minorAmountError: { $set: e.target.validity.patternMismatch } },
				}),
			}
		})
	}

	save = () => {
		if (
			this.state.newTransactions.every(transaction => {
				return newTransactionIsValid(transaction)
			})
		) {
			this.props
				.saveBulkTransactions(this.state.newTransactions)
				.then(() => {
					toast.success('Save success!', { className: 'success' })
					this.props.history.replace('/')
				})
				.catch(err => {
					console.log('error', err)
					toast.error('Server error saving transaction', { className: 'error' })
				})
		}
	}
}

function mapStateToProps(state) {
	return {
		methods: state.transactions.methods,
		categories: state.transactions.categories,
		currencies: state.transactions.currencies,
		user: state.user,
	}
}

const mapDispatchToProps = {
	saveBulkTransactions,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(UploadFile)

import createStore from '../createStore'
import CommunicatorType from '../../services/Communicator'
import { getTransactions } from '../transaction'

import type { Dispatch } from 'redux'
import type { Action, UserStore } from '../types'

const types = {
	UPDATE_USER: 'UPDATE_USER',
}

export function getUser() {
	return (dispatch: Dispatch<any>, getState: () => any, Communicator: typeof CommunicatorType) => {
		Communicator.GET('users/current_user')
			.then(({ user }) => {
				dispatch(updateUser(user))

				dispatch(getTransactions(user.id))
			})
			.catch(err => {}) // nothing, just means they need to log in.
	}
}

export function login(email: string, password: string) {
	return (dispatch: Dispatch<any>, getState: () => any, Communicator: typeof CommunicatorType) => {
		Communicator.POST('users/sign_in', { body: { email, password } })
			.then(({ user }) => {
				if (user) {
					dispatch(updateUser(user))
				} else {
					console.log('Error, result had no user data')
				}
			})
			.catch(err => {
				console.log(err)
			})
	}
}

export function register(email: string, password: string, first_name: string, last_name: string) {
	return (dispatch: Dispatch<any>, getState: () => any, Communicator: typeof CommunicatorType) => {
		const body = {
			user: {
				email,
				password,
				first_name,
				last_name,
			},
		}
		Communicator.POST('users', { body })
			.then(({ user }) => {
				if (user) {
					dispatch(updateUser(user))
				} else {
					console.log('Error, result had no user data')
				}
			})
			.catch(err => {
				console.log(err)
			})
	}
}

export function updateUser(user: UserStore): Action {
	return {
		type: types.UPDATE_USER,
		payload: user,
	}
}

// reducers
const initialState: ?UserStore = null

export default createStore(initialState, {
	[types.UPDATE_USER]: (state, { payload: user }) => {
		return user
	},
})

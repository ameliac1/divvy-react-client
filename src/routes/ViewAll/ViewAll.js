import React, { Component } from 'react'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import { FilterableTable } from './FilterableTable'
import type { Transaction, IdMap } from '../../store/types'
import { deleteTransaction } from '../../store/transaction'
import './ViewAll.css'

type Props = {
	transactions: IdMap<Transaction>,
	deleteTransaction: (transactionId: string) => void,
}

type State = {
	filterText: string,
}

class ViewAll extends Component<Props, State> {
	state = {
		filterText: '',
	}

	render() {
		let allTransactions = Object.values(this.props.transactions)
		allTransactions.sort((a, b) => {
			return new Date(b.date) - new Date(a.date)
		})

		return (
			<div className="ViewAll">
				<h1>Manage Transactions</h1>
				<div className="fieldset">
					<div className="placeholder">Filter</div>
					<input
						type="text"
						value={this.state.filterText}
						onChange={e => this.setState({ filterText: e.target.value })}
					/>
				</div>
				<FilterableTable
					className="transactions-table"
					headers={[
						{
							content: '',
							className: 'actions',
						},
						{
							content: 'Date',
							className: 'date',
						},
						{
							content: 'Title',
							className: 'title',
						},
						{
							content: 'Amount',
							className: 'amount',
						},
						{
							content: 'Category',
							className: 'category',
						},
						{
							content: 'Method',
							className: 'method',
						},
					]}
					elements={allTransactions}
					filterText={this.state.filterText}
					filterableAttributes={['title']}
					deleteTransaction={this.deleteTransaction}
				/>
			</div>
		)
	}

	deleteTransaction = (id: string) => {
		this.props.deleteTransaction(id).catch(err => {
			toast.error('Server error- could not remove transaction', { className: 'error' })
		})
	}
}

function mapStateToProps(state) {
	return {
		transactions: state.transactions.all,
	}
}

const mapDispatchToProps = {
	deleteTransaction,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ViewAll)

// @flow
import { combineReducers } from 'redux'

import userReducer from './user'
import transactionReducer from './transaction'

import type { UserStore, TransactionStore } from './types'

export type ReduxStore = {
	user: ?UserStore,
	transactions: TransactionStore,
}

export default function createRootReducer() {
	return combineReducers({
		user: userReducer,
		transactions: transactionReducer,
	})
}

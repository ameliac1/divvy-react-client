import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { ToastContainer, Slide } from 'react-toastify'
import { Login, Sidebar } from './components'
import { Dashboard, AddTransaction, ViewAll, UploadFile } from './routes'
import type { User } from './store/types'
import './main.css'

type Props = {
	user: User,
}

class Main extends Component<Props> {
	render() {
		return (
			<Router>
				<div className="Main">
					<Sidebar />
					<div className="main-content">
						{this.props.user ? (
							<React.Fragment>
								<Route exact path="/" component={ViewAll} />
								<Route path="/analytics" component={Dashboard} />
								<Route path="/new" component={AddTransaction} />
								<Route path="/upload" component={UploadFile} />
							</React.Fragment>
						) : (
							<Login />
						)}
					</div>
					<ToastContainer
						position="bottom-right"
						className="toasts"
						autoClose={5000}
						hideProgressBar
						newestOnTop
						closeOnClick
						rtl={false}
						transition={Slide}
						pauseOnVisibilityChange
						pauseOnHover
						toastClassName="app-toast"
					/>
				</div>
			</Router>
		)
	}
}

function mapStateToProps(state) {
	return {
		user: state.user,
	}
}

export default connect(mapStateToProps)(Main)

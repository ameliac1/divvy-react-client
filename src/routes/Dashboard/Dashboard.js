import React, { Component } from 'react'
import { connect } from 'react-redux'
import { IdMap, Transaction } from '../../store/types'
import { PieChartHelper } from '../../components/charts'
import './Dashboard.css'

type Props = {
	transactionsByCategory: IdMap<Transaction[]>,
	income: number,
	spending: number,
	savings: number,
}

/*
	Notes and disclaimers:
	1) This is a very simple idea of a dashboard. There are some obvious problems,
		such as currency issues. The total spent/recieved would be incorrect without
		converting all transactions to a preferred currency. That seemed beyond the
		scope of this though, so I left this out.
	2) Dates would be nice (view spending by categoru between Jan 1st and March
		20th, etc.) but were outside of the scope of this since other featueres were
		implemented instead.
	2) Animations would be nice, but again outside of scope.
*/

class Dashboard extends Component<Props> {
	render() {
		return (
			<div className="Dashboard">
				<h1>Analytics</h1>
				<div>
					This month, you made {this.props.income} and spent {this.props.spending}
					{this.props.savings}
				</div>
				<PieChartHelper
					data={{
						label: 'Categories',
						values: Object.values(this.props.transactionsByCategory),
					}}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	let categories = {}
	let spending = 0
	Object.values(state.transactions.all).forEach(transaction => {
		if (!categories[transaction.category.name])
			categories[transaction.category.name] = {
				x: transaction.category.name,
				majorAmount: 0,
				minorAmount: 0,
			}

		categories[transaction.category.name].majorAmount += transaction.majorAmount
		categories[transaction.category.name].minorAmount += transaction.minorAmount
	})

	Object.values(categories).forEach(category => {
		const amount = category.majorAmount + category.minorAmount / 100
		if (category.x !== 'Income') spending += amount
		category.y = amount.toFixed(2)
		delete category.majorAmount
		delete category.minorAmount
	})

	return {
		transactionsByCategory: categories,
		spending: spending.toFixed(2),
		income: categories['Income'] ? categories['Income'].y : 0,
	}
}

export default connect(mapStateToProps)(Dashboard)

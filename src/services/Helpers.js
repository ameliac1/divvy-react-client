import type { NewTransaction } from '../store/types'

type Key = $Subtype<string>

/**
 * The array find function implemented for object. Will return the first value in the object
 * that passes the test provided by the callback.
 * @param  {Object}   object   The object
 * @param  {Function} callback The callback function
 */
export function findObject<T>(
	object: { [Key]: T },
	callback: (currentValue: T, key: Key) => boolean
): ?T {
	const objectKey = Object.keys(object).find(key => {
		return object.hasOwnProperty(key) && callback(object[key], key)
	})
	return objectKey ? object[objectKey] : null
}

// NOTE: to really validate we probably want to check that the currency, category, and methods are valid and not that they simply exist.
export function newTransactionIsValid(t: NewTransaction) {
	return (
		t.date &&
		typeof t.date.getMonth === 'function' &&
		t.title &&
		t.majorAmount &&
		t.minorAmount &&
		t.minorAmount.length === 2 &&
		t.currency &&
		t.category &&
		t.method &&
		t.user
	)
}

import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import { getDivider } from '../../services/CurrencyService'
import type { Transaction, Category, Method, Currency, IdMap } from '../../store/types'
import './UploadFile.css'

type Props = {
	transaction: Transaction,
	categories: IdMap<Category>,
	methods: IdMap<Method>,
	currencies: IdMap<Currency>,
	handleDateChange: (date: Date) => void,
	handleFieldChange: (fieldName: string) => void,
}

export default class NewTransaction extends Component<Props> {
	render() {
		const { transaction, handleDateChange, handleFieldChange } = this.props
		return (
			<div className="NewTransactionRow">
				<div className="fieldset">
					<div className="placeholder">Date</div>
					<DatePicker
						selected={transaction.date}
						onChange={handleDateChange}
						maxDate={new Date()}
					/>
				</div>
				<div className="fieldset">
					<div className="placeholder">Title</div>
					<input type="text" value={transaction.title} onChange={handleFieldChange('title')} />
				</div>
				<div className="fieldset">
					<div className="placeholder amount-header">Amount</div>
					<div className="row amount-row">
						<span
							dangerouslySetInnerHTML={{
								__html: transaction.currency.unicode,
							}}
						/>
						<input
							type="number"
							min="0"
							className="major-amount"
							value={transaction.majorAmount}
							onChange={handleFieldChange('majorAmount')}
						/>
						<span>{getDivider(transaction.currency)}</span>
						<input
							className={transaction.minorAmountError ? 'minor-amount error' : 'minor-amount'}
							type="text"
							maxLength="2"
							pattern="[0-9]{2}"
							title="(.00 to .99)"
							value={transaction.minorAmount}
							onChange={handleFieldChange('minorAmount')}
						/>
					</div>
				</div>
				<div className="fieldset">
					<div className="placeholder">Currency</div>
					<select
						name="Currency"
						onChange={handleFieldChange('currency')}
						value={transaction.currency.code}>
						{Object.values(this.props.currencies).map(currency => {
							return (
								<option value={currency.code} key={currency.id}>
									{`${currency.code} - ${currency.name}`}
								</option>
							)
						})}
					</select>
				</div>
				<div className="fieldset">
					<div className="placeholder">Method</div>
					<select
						name="Method"
						onChange={handleFieldChange('method')}
						value={transaction.method.name}>
						{Object.values(this.props.methods).map(method => {
							return (
								<option value={method.name} key={method.id}>
									{method.name}
								</option>
							)
						})}
					</select>
				</div>
				<div className="fieldset">
					<div className="placeholder">Category</div>
					<select
						name="Category"
						onChange={handleFieldChange('category')}
						value={transaction.category.name}>
						{Object.values(this.props.categories).map(category => {
							return (
								<option value={category.name} key={category.id}>
									{category.name}
								</option>
							)
						})}
					</select>
				</div>
				<div className="fieldset">
					<div className="placeholder">Note</div>
					<input type="text" value={transaction.note} onChange={handleFieldChange('note')} />
				</div>
			</div>
		)
	}
}

// @flow
import type { Action } from './types'

export default function createStore<State>(
	initialState: State,
	handlers: { [string]: (State, Action) => State }
) {
	return (state: State = initialState, action: Action) => {
		return handlers[action.type] ? handlers[action.type](state, action) : state
	}
}

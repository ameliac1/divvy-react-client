import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import type { User } from '../../store/types'

import { FiPlus, FiList, FiBarChart2, FiFilePlus } from 'react-icons/fi'
import './Sidebar.css'

type Props = {
	user: User,
}

class Sidebar extends Component<Props> {
	render() {
		return (
			<div className="Sidebar">
				{this.props.user && (
					<React.Fragment>
						<div className="user-profile">
							Welcome, {this.props.user.firstName} {this.props.user.lastName}
						</div>
						<Link to="/" className="sidebar-element">
							<FiList className="icon" />
							<div>View All</div>
						</Link>
						<Link to="/analytics" className="sidebar-element">
							<FiBarChart2 className="icon" />
							<div>Analytics</div>
						</Link>
						<Link to="/new" className="sidebar-element">
							<FiPlus className="icon" />
							<div>Add New</div>
						</Link>
						<Link to="/upload" className="sidebar-element">
							<FiFilePlus className="icon" />
							<div>Upload File</div>
						</Link>
					</React.Fragment>
				)}
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		user: state.user,
	}
}

export default connect(mapStateToProps)(Sidebar)

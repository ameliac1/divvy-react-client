// @flow
import React, { Component } from 'react'
import { toast } from 'react-toastify'
import CSVReader from 'react-csv-reader'

type Props = {
	onLoad: any,
	exampleData: Object[],
}

const masterKeys = ['title', 'date', 'dollars', 'cents', 'currency', 'method', 'category', 'note']

/*
File Loader

Functionality:
    Allows the user to upload a .csv file. Parses the file into a matrix, and
    parseMatrix() takes it from an array of arrays to an array of objects
    dictated by the masterKeys. The objects will contain all of
    the attributes that are found in the intersection of masterKeys and the
    first line of the uploaded .csv file.

    Note: order of the columns does not matter, only that the headers are over
    the correct columns
*/

export default class FileLoader extends Component<Props> {
	parseMatrix = (rows: any) => {
		const fileKeys = rows.splice(0, 1)[0]
		let parsedRows = []
		rows.forEach(row => {
			if (row[0] !== '') {
				parsedRows.push(this.parseRow(row, fileKeys))
			}
		})

		this.props.onLoad(parsedRows)
	}

	parseRow = (row: string[], fileKeys: string[]) => {
		let newRow = {}
		fileKeys
			.map(key => key.toLowerCase())
			.forEach((fileKey, i) => {
				if (masterKeys.indexOf(fileKey) > -1) {
					newRow[fileKey] = row[i]
				}
			})

		masterKeys.forEach(key => {
			if (!newRow[key]) {
				newRow[key] = ''
			}
		})

		return newRow
	}

	handleError = (error: any) => {
		toast.error('Error uploading .csv! Please ensure it is formatted correctly.', {
			autoClose: 8000,
			className: 'error',
		})
	}

	render() {
		return (
			<div className="csv-upload-container">
				<a
					className="slidey-sides"
					target="_blank"
					rel="noopener noreferrer"
					download="example-transaction.csv"
					href={this.getEncodedURI()}>
					<span>See Example</span>
				</a>
				<CSVReader
					cssClass="csv-input"
					onFileLoaded={this.parseMatrix}
					onError={this.handleError}
				/>
			</div>
		)
	}

	getEncodedURI = () => {
		let csv = 'data:text/csv;charset=utf-8,'
		this.props.exampleData.forEach((row, i) => {
			let csvRow = row.join(',')
			csv += csvRow
			if (i !== this.props.exampleData.length - 1) {
				csv += '\r\n'
			}
		})
		return encodeURI(csv)
	}
}

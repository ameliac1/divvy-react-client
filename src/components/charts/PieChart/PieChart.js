import React, { Component } from 'react'
import { PieChart } from 'react-d3-components'
import './PieChart.css'

type Props = {
	data: any,
	// Note: could add date selection to track spending within a specific date range
}

/*
	Disclaimers and notes:
	Normally, I would want to write my own component for a chart using d3 and not rely
	on a third-party open source pacakge. A self-managed package is generally more
	reliable, and you can customize it however you like. For the purpose of this
	activity however, I chose to use react-d3-components to get something up quickly.

	I didn't bother to customize it much, because again I would rather normally make my own.

*/

export default class PieChartHelper extends Component<Props> {
	render() {
		return (
			<div className="PieChart">
				<PieChart
					data={this.props.data}
					width={600}
					height={400}
					margin={{ top: 10, bottom: 10, left: 100, right: 100 }}
					tooltipHtml={this.getTooltip}
				/>
			</div>
		)
	}

	getTooltip(label, value) {
		return `${label} ${value}`
	}
}

const COMMA_COUNTRIES = ['EUR', 'HUF', 'SEK', 'VND']

function getDivider(currencyCode: string) {
	return COMMA_COUNTRIES.includes(currencyCode) ? ',' : '.'
}

module.exports = {
	getDivider,
}

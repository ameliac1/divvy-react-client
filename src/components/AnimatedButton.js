import React, { Component } from 'react'
import '../Buttons.css'

type Props = {
	type: string,
	content: any,
	onClick?: Function,
	disabled?: boolean,
	className?: string,
}

export default class AnimatedButton extends Component<Props> {
	render() {
		let { type, content, onClick, className, disabled } = this.props
		let classes = 'slidey-sides'
		if (className) classes += ` ${className}`

		return (
			<button
				className={classes}
				onClick={() => {
					if (typeof onClick === 'function') onClick()
				}}
				type={type}
				disabled={disabled}>
				<span>{content}</span>
			</button>
		)
	}
}

import React, { Component } from 'react'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import DatePicker from 'react-datepicker'
import type { NewTransaction, IdMap, Option, Currency, User } from '../../store/types'
import { AnimatedButton } from '../../components'
import { saveTransaction } from '../../store/transaction'
import { formatDateShort } from '../../services/DateService'
import { getDivider } from '../../services/CurrencyService'
import { toast } from 'react-toastify'
import {
	FiGift,
	FiShoppingCart,
	FiMapPin,
	FiTv,
	FiZap,
	FiHome,
	FiDollarSign,
	FiBookOpen,
	FiPlus,
	FiShoppingBag,
	FiUser,
	FiLoader,
	FiChevronsRight,
	FiMoreHorizontal,
	FiCalendar,
} from 'react-icons/fi'
import './AddTransaction.css'

type Props = {
	saveTransaction: NewTransaction => mixed,
	categories: IdMap<Option>,
	methods: IdMap<Option>,
	currencies: IdMap<Currency>,
	user: User,
	history: Object,
}

type State = {
	transaction: NewTransaction,
	stepIndex: number,
	minorAmountError: boolean,
}

class AddTransaction extends Component<Props, State> {
	constructor(props: Props) {
		super(props)
		this.state = {
			transaction: {
				date: new Date(),
				title: '',
				note: '',
				majorAmount: 0,
				minorAmount: '00',
				category: null,
				currency: {
					id: 10,
					code: 'USD',
					name: 'United States Dollar',
					unicode: '&#x24;',
				},
				method: {
					id: 1,
					name: 'Credit',
				},
				user: props.user,
			},
			stepIndex: 0,
			minorAmountError: false,
		}
	}

	render() {
		return (
			<div className="AddTransaction">
				<h1>Add Transactions</h1>
				<p className="note">
					Enter your transactions to track your spending. If you have multiple to enter, try
					uploading a file (on the {"'"}Upload File{"'"} page) to expedite the process.
				</p>
				<div className="new-transaction-header row">
					<AnimatedButton
						content="Back"
						onClick={this.goBack}
						disabled={this.state.stepIndex < 1}
					/>
					{this.renderProgressPath()}
					{this.state.stepIndex < 2 ? (
						<AnimatedButton content="Next" onClick={this.goNext} disabled={this.getIsDisabled()} />
					) : (
						<AnimatedButton content="Save" onClick={this.save} disabled={this.getIsDisabled()} />
					)}
				</div>
				<div className="step">
					{this.state.stepIndex === 0 ? (
						<div className="categories-container">
							{Object.values(this.props.categories).map(category => {
								const { name, id } = category
								return (
									<div className="category" key={id} onClick={() => this.selectCategory(category)}>
										{this.getCategoryIcon(name)}
										<span>{name}</span>
									</div>
								)
							})}
						</div>
					) : this.state.stepIndex === 1 ? (
						<React.Fragment>
							<div className="fieldset">
								<div className="placeholder">Title</div>
								<input
									type="text"
									placeholder=" "
									value={this.state.transaction.title}
									onChange={this.handleFieldChange('title')}
								/>
							</div>
							<div className="row">
								<div className="fieldset">
									<div className="placeholder">Currency</div>
									<select
										name="Currency"
										onChange={this.handleFieldChange('currency')}
										value={this.state.transaction.currency.code}>
										{Object.values(this.props.currencies).map(currency => {
											return (
												<option value={currency.code} key={currency.id}>
													{`${currency.code} - ${currency.name}`}
												</option>
											)
										})}
									</select>
								</div>
								<div className="fieldset">
									<div className="placeholder">Amount</div>
									<div className="row amount-row">
										<input
											type="number"
											min="0"
											value={this.state.transaction.majorAmount}
											onChange={this.handleFieldChange('majorAmount')}
										/>
										<span>{getDivider(this.state.transaction.currency)}</span>
										<input
											className={this.state.minorAmountError ? 'error' : ''}
											type="text"
											maxLength="2"
											ref={this.myRef}
											pattern="[0-9]{2}"
											title="(.00 to .99)"
											value={this.state.transaction.minorAmount}
											onChange={this.handleFieldChange('minorAmount')}
										/>
									</div>
								</div>
								<div className="fieldset">
									<div className="placeholder">Method</div>
									<select
										name="Method"
										onChange={this.handleFieldChange('method')}
										value={this.state.transaction.method.id}>
										{Object.values(this.props.methods).map(method => {
											return (
												<option value={method.id} key={method.id}>
													{method.name}
												</option>
											)
										})}
									</select>
								</div>
							</div>
							<div className="fieldset">
								<div className="placeholder">Date</div>
								<DatePicker
									selected={this.state.transaction.date}
									onChange={this.handleDateChange}
									maxDate={new Date()}
								/>
							</div>
							<div className="fieldset">
								<div className="placeholder">Extra Notes</div>
								<input
									type="text"
									value={this.state.transaction.note}
									onChange={this.handleFieldChange('note')}
								/>
							</div>
						</React.Fragment>
					) : (
						<div className="confirm-page column">
							<p>
								Is this information correct? If so, click {"'"}Save{"'"}. Otherwise, go back and
								make any needed changes.
							</p>
							<div className="new-transaction-review">
								<h1>
									<span
										dangerouslySetInnerHTML={{
											__html: this.state.transaction.currency.unicode,
										}}
									/>
									<span>
										{' '}
										{this.state.transaction.majorAmount}
										{getDivider(this.state.transaction.currency)}
										{this.state.transaction.minorAmount}
									</span>
								</h1>
								<div className="row">
									<div className="date">
										<FiCalendar />
										<span>{formatDateShort(this.state.transaction.date)}</span>
									</div>
									<div>Method: {this.state.transaction.method.name}</div>
								</div>
								<h3>{this.state.transaction.title}</h3>
								<span className="transaction-category">
									{this.state.transaction.category.name}{' '}
									{this.getCategoryIcon(this.state.transaction.category.name)}
								</span>
								{this.state.transaction.note && (
									<div className="note">Note: {this.state.transaction.note}</div>
								)}
							</div>
							<AnimatedButton content="Save" onClick={this.save} />
						</div>
					)}
				</div>
			</div>
		)
	}

	convertUnicode(input) {
		return input.replace(/\\u(\w\w\w\w)/g, (a, b) => {
			var charcode = parseInt(b, 16)
			return String.fromCharCode(charcode)
		})
	}

	goBack = () => {
		this.setState(state => {
			return {
				stepIndex: state.stepIndex - 1,
			}
		})
	}

	goNext = () => {
		this.setState(state => {
			return {
				stepIndex: state.stepIndex + 1,
			}
		})
	}

	save = () => {
		this.props
			.saveTransaction(this.state.transaction)
			.then(() => {
				toast.success('Save success!', { className: 'success' })
				this.props.history.replace('/')
			})
			.catch(err => {
				console.log('error', err)
				toast.error('Server error saving transaction', { className: 'error' })
			})
	}

	getIsDisabled() {
		const { transaction, stepIndex } = this.state
		switch (stepIndex) {
			case 0:
				return !transaction.category
			case 1:
				return !(
					transaction.date &&
					transaction.title &&
					transaction.majorAmount > -1 &&
					transaction.minorAmount &&
					transaction.method &&
					transaction.category &&
					!this.state.minorAmountError
				)
			default:
				return false
		}
	}

	getCategoryIcon(categoryName: string) {
		switch (categoryName) {
			case 'Gifts':
				return <FiGift />
			case 'Food':
				return <FiShoppingCart />
			case 'Travel':
				return <FiMapPin />
			case 'Entertainment':
				return <FiTv />
			case 'Utilities':
				return <FiZap />
			case 'Housing':
				return <FiHome />
			case 'Income':
				return <FiDollarSign />
			case 'Education':
				return <FiBookOpen />
			case 'Health Care':
				return <FiPlus />
			case 'Clothing':
				return <FiShoppingBag />
			case 'Personal Care':
				return <FiUser />
			case 'Miscellaneous':
				return <FiLoader />
			default:
				return <FiMoreHorizontal />
		}
	}

	selectCategory(category: Option) {
		this.setState(state => {
			return {
				transaction: {
					...state.transaction,
					category: category,
				},
				stepIndex: state.stepIndex + 1,
			}
		})
	}

	renderProgressPath() {
		const { transaction, stepIndex } = this.state
		return (
			<div className="progress-path row">
				<h3 onClick={() => this.setState({ stepIndex: 0 })}>
					Choose Category{transaction.category && `: ${transaction.category.name}`}
				</h3>
				{stepIndex > 0 && (
					<React.Fragment>
						<FiChevronsRight />
						<h3 onClick={() => this.setState({ stepIndex: 1 })}>Add Details</h3>
					</React.Fragment>
				)}
				{stepIndex > 1 && (
					<React.Fragment>
						<FiChevronsRight />
						<h3>Review</h3>
					</React.Fragment>
				)}
			</div>
		)
	}

	handleDateChange = (date: Date) => {
		this.setState(state => {
			return {
				transaction: {
					...state.transaction,
					date: date,
				},
			}
		})
	}

	handleFieldChange = (fieldName: string) => (e: MessageEvent) => {
		e.persist() // weird error was happenin
		let newValue
		if (fieldName === 'currency') {
			newValue = this.props.currencies[e.target.value]
		} else if (fieldName === 'method') {
			newValue = this.props.methods[e.target.value]
		} else {
			newValue = e.target.value
		}
		this.setState(state => {
			return {
				minorAmountError: e.target.validity.patternMismatch,
				transaction: update(state.transaction, { [fieldName]: { $set: newValue } }),
			}
		})
	}
}

function mapStateToProps(state) {
	return {
		categories: state.transactions.categories,
		methods: state.transactions.methods,
		currencies: state.transactions.currencies,
		user: state.user,
	}
}

const mapDispatchToProps = {
	saveTransaction,
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddTransaction)

// @flow
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import createRootReducer from './rootReducer'
import Communicator from '../services/Communicator'
export type { ReduxStore } from './rootReducer'

const initialState = {}
const enhancers = []
const middleware = [thunk.withExtraArgument(Communicator)]

if (process.env.NODE_ENV === 'development') {
	middleware.push(logger)
}

const composedEnhancers = composeWithDevTools(applyMiddleware(...middleware), ...enhancers)

const store = createStore(createRootReducer(), initialState, composedEnhancers)

export default store

type RequestOptions = {
	method?: 'GET' | 'PUT' | 'POST' | 'PATCH' | 'DELETE',
	body?: string | Object,
	headers?: { [key: string]: string },
	host?: string,
}

const API_PREFIX = 'http://localhost:4000/api/'

export default class Communicator {
	static GET(path: string, options?: RequestOptions): Promise<Object> {
		return this.request(path, { ...options, method: 'GET' })
	}

	static PUT(path: string, options?: RequestOptions): Promise<Object> {
		return this.request(path, { ...options, method: 'PUT' })
	}

	static POST(path: string, options?: RequestOptions): Promise<Object> {
		return this.request(path, { ...options, method: 'POST' })
	}

	static DELETE(path: string, options?: RequestOptions): Promise<Object> {
		return this.request(path, { ...options, method: 'DELETE' })
	}

	static PATCH(path: string, options?: RequestOptions): Promise<Object> {
		return this.request(path, { ...options, method: 'PATCH' })
	}

	static request(path: string, options: RequestOptions): Promise<Object> {
		if (options.body && typeof options.body !== 'string') {
			options.body = JSON.stringify(options.body)
		}

		const url = options.host ? options.host + path : API_PREFIX + path
		delete options.host

		options = {
			...options,
			credentials: 'include',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			redirect: 'follow',
		}

		let response: Response
		let responseOK
		return fetch(url, options)
			.then((result: Response) => {
				response = result
				responseOK = result.ok
				const contentType = response.headers.get('content-type')
				if (contentType && contentType.includes('application/json')) {
					return result.json()
				}
				result.text().then(body => {
					console.error(body)
					throw new TypeError('Did not receive JSON :(')
				})
			})
			.then(result => {
				if (!responseOK) {
					// eslint-disable-next-line no-throw-literal
					throw {
						...result,
						headers: response.headers,
						status: response.status,
					}
				}

				return result
			})
	}
}

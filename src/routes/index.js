import Dashboard from './Dashboard'
import AddTransaction from './AddTransaction'
import ViewAll from './ViewAll'
import UploadFile from './UploadFile'

export { Dashboard, AddTransaction, ViewAll, UploadFile }

import createStore from '../createStore'
import type { Dispatch } from 'redux'
import CommunicatorType from '../../services/Communicator'
import type {
	Action,
	TransactionStore,
	Transaction,
	NewTransaction,
	GetState,
	Option,
	Currency,
} from '../types'
import update from 'immutability-helper'

const types = {
	UPDATE_TRANSACTION: 'UPDATE_TRANSACTION',
	REMOVE_TRANSACTION: 'REMOVE_TRANSACTION',
	ADD_TRANSACTIONS: 'ADD_TRANSACTIONS',
	SET_METHODS: 'SET_METHODS',
	SET_CATEGORIES: 'SET_CATEGORIES',
	SET_CURRENCIES: 'SET_CURRENCIES',
}

export function getTransactions(userId: string) {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		Communicator.GET(`users/${userId}/transactions`)
			.then(({ transactions }) => {
				transactions.forEach(transaction => {
					dispatch(updateTransaction(transaction))
				})
			})
			.catch(err => {
				console.log('Error: ', err) // TODO: deal with errors gracefully
			})
	}
}

// Returns a transaction with id associations for method, category, user, and currency
function cleanTransaction(transaction: NewTransaction) {
	return {
		title: transaction.title,
		date: transaction.date,
		note: transaction.note,
		major_amount: transaction.majorAmount,
		minor_amount: transaction.minorAmount,
		currency_id: transaction.currency.id,
		method_id: transaction.method.id,
		category_id: transaction.category.id,
		user_id: transaction.user.id,
	}
}

export function saveTransaction(transaction: NewTransaction) {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		const cleanedTransaction = cleanTransaction(transaction)
		console.log('Cleaned Transaction: ', cleanedTransaction)
		return Communicator.POST(`transaction`, { body: { transaction: cleanedTransaction } }).then(
			data => {
				dispatch(
					updateTransaction({
						...transaction,
						id: data.id,
						majorAmount: parseInt(data.majorAmount, 10),
						minorAmount: parseInt(data.minorAmount, 10),
					})
				)
			}
		)
	}
}

export function saveBulkTransactions(newTransactions: NewTransaction[]) {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		const cleanedTransactions = newTransactions.map(transaction => cleanTransaction(transaction))
		return Communicator.POST(`transactions`, { body: { transactions: cleanedTransactions } }).then(
			({ transactions }) => {
				dispatch(addTransactions(transactions))
			}
		)
	}
}

export function editTransaction(transaction: Transaction) {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		if (transaction.id) {
			return Communicator.PUT(`transactions/${transaction.id}`, { body: transaction })
				.then(data => {
					dispatch(updateTransaction(data.transaction))
				})
				.catch(err => {
					console.log('Error: ', err) // TODO: deal with errors gracefully
				})
		}
	}
}

export function deleteTransaction(id: string) {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		return Communicator.DELETE(`transactions/${id}`).then(({ id }) => {
			dispatch(removeTransaction(id))
		})
	}
}

export function getCategories() {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		Communicator.GET(`categories`)
			.then(({ categories }) => {
				dispatch(addCategories(categories))
			})
			.catch(err => {
				console.log('Error: ', err) // TODO: deal with errors gracefully
			})
	}
}

export function getMethods() {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		Communicator.GET(`methods`)
			.then(({ methods }) => {
				dispatch(setMethods(methods))
			})
			.catch(err => {
				console.log('Error: ', err) // TODO: deal with errors gracefully
			})
	}
}

export function getCurrencies() {
	return (dispatch: Dispatch<*>, getState: GetState, Communicator: typeof CommunicatorType) => {
		Communicator.GET(`currencies`)
			.then(({ currencies }) => {
				dispatch(setCurrencies(currencies))
			})
			.catch(err => {
				console.log('Error: ', err) // TODO: deal with errors gracefully
			})
	}
}

export function updateTransaction(transaction: Transaction): Action {
	return {
		type: types.UPDATE_TRANSACTION,
		payload: transaction,
	}
}

export function addTransactions(transactions: Transaction[]): Action {
	return {
		type: types.ADD_TRANSACTIONS,
		payload: transactions,
	}
}

export function addCategories(categories: Option[]): Action {
	return {
		type: types.SET_CATEGORIES,
		payload: categories,
	}
}

export function setMethods(methods: Option[]): Action {
	return {
		type: types.SET_METHODS,
		payload: methods,
	}
}

export function setCurrencies(currencies: Currency[]): Action {
	return {
		type: types.SET_CURRENCIES,
		payload: currencies,
	}
}

export function removeTransaction(id: string): Action {
	return {
		type: types.REMOVE_TRANSACTION,
		payload: id,
	}
}

// reducers
const initialState: TransactionStore = {
	all: {},
	categories: {},
	methods: {},
	currencies: {},
}

export default createStore(initialState, {
	[types.UPDATE_TRANSACTION]: (state, { payload: transaction }) => {
		return {
			...state,
			all: update(state.all, { [transaction.id]: { $set: transaction } }),
		}
	},
	[types.ADD_TRANSACTIONS]: (state, { payload: transactions }) => {
		let storeTransactions = {}
		transactions.forEach(t => {
			storeTransactions[t.id] = {
				id: t.id,
				date: t.date,
				title: t.title,
				majorAmount: parseInt(t.majorAmount, 10),
				minorAmount: parseInt(t.minorAmount, 10),
				currency:
					Object.values(state.currencies).find(currency => t.currency_id === currency.id) ||
					state.currencies[10],
				category:
					Object.values(state.categories).find(category => t.category_id === category.id) ||
					state.categories[12],
				method:
					Object.values(state.methods).find(method => t.method_id === method.id) ||
					state.methods[1],
				note: t.note,
				user: { id: t.user_id },
			}
		})
		return {
			...state,
			all: {
				...state.all,
				...storeTransactions,
			},
		}
	},
	[types.REMOVE_TRANSACTION]: (state, { payload: id }) => {
		return {
			...state,
			all: update(state.all, { $unset: [id] }),
		}
	},
	[types.SET_CATEGORIES]: (state, { payload: categories }) => {
		let mappedCategories = {}
		categories.forEach(category => {
			mappedCategories[category.id] = category
		})
		return {
			...state,
			categories: mappedCategories,
		}
	},
	[types.SET_METHODS]: (state, { payload: methods }) => {
		let mappedMethods = {}
		methods.forEach(method => {
			mappedMethods[method.id] = method
		})
		return {
			...state,
			methods: mappedMethods,
		}
	},
	[types.SET_CURRENCIES]: (state, { payload: currencies }) => {
		let mappedCurrencies = {}
		currencies.forEach(currency => {
			mappedCurrencies[currency.code] = currency
		})
		return {
			...state,
			currencies: mappedCurrencies,
		}
	},
})

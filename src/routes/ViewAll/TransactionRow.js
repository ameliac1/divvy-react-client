import React, { Component } from 'react'
// import { connect } from 'react-redux'
import { formatStringDateShort } from '../../services/DateService'
import { getDivider } from '../../services/CurrencyService'
import type { Transaction } from '../../store/types'
import { FiTrash2 } from 'react-icons/fi'

type Props = {
	transaction: Transaction,
	removeTransaction: () => void,
}

export default class TransactionRow extends Component<Props> {
	render() {
		const {
			date,
			title,
			majorAmount,
			minorAmount,
			currency,
			category,
			method,
			note,
		} = this.props.transaction
		return (
			<div className="TransactionRow column">
				<div className="row">
					<div className="actions">
						<FiTrash2 onClick={this.props.removeTransaction} />
					</div>
					<div className="date">{formatStringDateShort(date)}</div>
					<div className="title">{title}</div>
					<div className="amount">
						<span
							dangerouslySetInnerHTML={{
								__html: currency.unicode,
							}}
						/>
						<span>{` ${majorAmount}`}</span>
						<span>{getDivider(currency.code)}</span>
						<span>{minorAmount < 10 ? `0${minorAmount}` : minorAmount}</span>
					</div>
					<div className="category">{category.name}</div>
					<div className="method">{method.name}</div>
				</div>
				{note && <div className="note">{`* ${note}`}</div>}
			</div>
		)
	}
}

// function mapStateToProps(state) {
// 	return {
// 		transactions: state.transactions.all,
// 	}
// }
//
// export default connect(mapStateToProps)(TransactionRow)
